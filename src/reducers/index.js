import { combineReducers } from 'redux';
import {
  STUDENT_LIST,
  STUDENT_CREATE,
  STUDENT_DETAIL,
  STUDENT_DELETE,
  STUDENT_PATCH,
} from '../actions/student';
import { PROJECT_LIST } from '../actions/projects';

function students(state = [], action) {
  switch (action.type) {
    case STUDENT_LIST:
      return action.data;
    default:
      return state;
  }
}

function projects(state = [], action) {
  switch (action.type) {
    case PROJECT_LIST:
      return action.data;
    default:
      return state;
  }
}

function studentDetail(state = [], action) {
  switch (action.type) {
    case STUDENT_DETAIL:
      return action.data;
    default:
      return state;
  }
}

function studentCreate(state = [], action) {
  switch (action.type) {
    case STUDENT_CREATE:
      return action.data;
    default:
      return state;
  }
}

function studenteDelete(state = [], action) {
  switch (action.type) {
    case STUDENT_DELETE:
      return action.data;
    default:
      return state;
  }
}

function studentPatch(state = [], action) {
  switch (action.type) {
    case STUDENT_PATCH:
      return action.data;
    default:
      return state;
  }
}

export default combineReducers({
  students,
  projects,
  studentDetail,
  studentCreate,
  studenteDelete,
  studentPatch,
});

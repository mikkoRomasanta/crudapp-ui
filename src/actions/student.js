import * as service from '../services/student';

/**
 * Get student list
 *
 */

export const STUDENT_LIST = 'STUDENT_LIST';

const dispatchGetStudents = (data) => ({
  type: STUDENT_LIST,
  data,
});

export const getStudents = () => {
  return (dispatch) => {
    return service
      .getStudentList()
      .then((res) => {
        dispatch(dispatchGetStudents(res));
      })
      .catch((e) => {
        throw e;
      });
  };
};

/**
 * Get student detail
 *
 */
export const STUDENT_DETAIL = 'STUDENT_DETAIL';

const dispatchGetStudentDetail = (data) => ({
  type: STUDENT_DETAIL,
  data,
});

export const getStudentDetail = (id) => {
  return (dispatch) => {
    return service
      .getDetail(id)
      .then((res) => {
        dispatch(dispatchGetStudentDetail(res));
      })
      .catch((e) => {
        throw e;
      });
  };
};

/**
 * Get student detail
 *
 */
export const STUDENT_CREATE = 'STUDENT_CREATE';

const dispatchPostStudent = (data) => ({
  type: STUDENT_CREATE,
  data,
});

export const postStudent = (data) => {
  return (dispatch) => {
    return service
      .postStudent(data)
      .then(() => {
        dispatch(dispatchPostStudent(data));
      })
      .catch((e) => {
        throw e;
      });
  };
};

/**
 * Patch student
 *
 */
export const STUDENT_DELETE = 'STUDENT_DELETE';

const dispatchDeleteStudents = (data) => ({
  type: STUDENT_DELETE,
  data,
});

export const deleteStudents = (id) => {
  return (dispatch) => {
    return service
      .deleteStudent(id)
      .then((res) => {
        dispatch(dispatchDeleteStudents(res));
        console.log('RES: ', res);
      })
      .catch((e) => {
        throw e;
      });
  };
};

export const STUDENT_PATCH = 'STUDENT_PATCH';

const dispatchPatchStudents = (data) => ({
  type: STUDENT_PATCH,
  data,
});

export const patchStudents = (id, body) => {
  return (dispatch) => {
    return service
      .patchStudent(id, body)
      .then((res) => {
        dispatch(dispatchPatchStudents(res));
      })
      .catch((e) => {
        throw e;
      });
  };
};

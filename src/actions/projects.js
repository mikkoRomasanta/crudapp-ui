import * as service from "../services/project"

export const PROJECT_LIST = "PROJECT_LIST"

const dispatchGetProjects = (data) => ({
    type: PROJECT_LIST,
    data,
})

/**
 * Get student list
 *
 */
export const getProjects = () => {
    return (dispatch) => {
        return service
            .getProjectList()
            .then((res) => {
                dispatch(dispatchGetProjects(res))
            })
            .catch((e) => {
                throw e
            })
    }
}

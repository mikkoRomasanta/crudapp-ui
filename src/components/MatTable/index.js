import { MaterialReactTable } from 'material-react-table';
import './styles.scss';
import { Box, IconButton } from '@mui/material';
import { Edit as EditIcon, Delete as DeleteIcon } from '@mui/icons-material';

// used for material react table. currently unused
// export default function SimpleTable({ data, column }) {
export default function SimpleTable2({ data, columns }) {
  return (
    <MaterialReactTable
      columns={columns}
      data={data}
      enableRowActions
      renderRowActions={({ row }) => (
        <Box>
          <IconButton onClick={() => console.info('Edit')}>
            <EditIcon />
          </IconButton>
          <IconButton onClick={() => console.info('Delete')}>
            <DeleteIcon />
          </IconButton>
        </Box>
      )}
    />
  );
}

import { Stack, TextField, Button } from "@mui/material"
import "./add-student.css"
import React from "react"
import axios from "axios"

function AddStudent() {
    const id_token = sessionStorage.getItem("id_token")
    const data = {
        first_name: "local",
        last_name: "test",
    }

    function addStudent() {
        axios({
            method: "post",
            url: "http://127.0.0.1:8000/api/student/",
            headers: {
                Authorization: "Bearer " + id_token,
            },
            data: data,
        }).then((response) => {
            console.log("RES: ", response)
        })
    }

    return (
        <div className="add-student">
            <Stack direction spacing={4} style={{ color: "white" }}>
                <Stack direction="row" spacing={2}>
                    <TextField label="FirstName" required></TextField>
                    <TextField label="LastName" required></TextField>
                    {/* <TextField
                        label="Department"
                        variant="filled"
                        required
                    ></TextField> */}
                    <TextField label="Enrollment Date" type="date"></TextField>
                </Stack>
                <Button onClick={addStudent()}>Submit</Button>
            </Stack>
        </div>
    )
}

export default AddStudent

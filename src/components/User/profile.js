import "./user.css"
import { useAuth0 } from "@auth0/auth0-react"

const ProfilePicture = () => {
    const { isAuthenticated, user } = useAuth0()

    return (
        isAuthenticated && (
            <img src={user.picture} alt={user.name + "profile pic"}></img>
        )
    )
}

export default ProfilePicture

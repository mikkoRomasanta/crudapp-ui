import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import { BrowserRouter, Link } from 'react-router-dom';
import AuthenticationButton from '../Login';
import ProfilePicture from '../User/profile';
import './styles.scss';

export default function Header() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar className="header">
        <Toolbar>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            <BrowserRouter>
              <Link className="header-link" to="/" reloadDocument={true}>
                CrudApp
              </Link>
            </BrowserRouter>
          </Typography>
          <ProfilePicture></ProfilePicture>
          <AuthenticationButton />
        </Toolbar>
      </AppBar>
    </Box>
  );
}

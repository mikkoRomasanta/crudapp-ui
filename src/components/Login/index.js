import './styles.scss';
import React from 'react';
import Button from '@mui/material/Button';

import { useAuth0 } from '@auth0/auth0-react';
import Cookies from 'js-cookie';

function Login() {
  const { loginWithRedirect } = useAuth0();

  return (
    <Button className="auth-button" onClick={() => loginWithRedirect()}>
      Log in
    </Button>
  );
}

function Logout() {
  const { logout } = useAuth0();

  return (
    <Button
      className="auth-button"
      onClick={() => {
        Cookies.remove('idToken');
        logout();
      }}
    >
      Log out
    </Button>
  );
}

const AuthenticationButton = () => {
  const { isAuthenticated } = useAuth0();

  return isAuthenticated ? <Logout /> : <Login />;
};

export default AuthenticationButton;

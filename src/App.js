import './App.scss';
import React from 'react';
import { Dashboard } from './pages/Dashboard';
import { Student } from './pages/Student';
import { StudentDetail } from './pages/StudentDetail';
import { StudentCreate } from './pages/StudentCreate';
import { Project } from './pages/Project';
// import "./App.css"
// import AddStudent from "./components/Student/add-student"
// import Login from "./components/Login/login"
import AuthenticationButton from './components/Login';
import { useAuth0 } from '@auth0/auth0-react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
// import GetIdToken from "./utils/get-id-token"
// import { useEffect, useState } from "react"
// import { useDispatch, useSelector } from "react-redux"
import Header from './components/Header';
// import { getToken } from "./utils/token"
import GetIdToken from './utils/get-id-token';
import Cookies from 'js-cookie';
// import { Container } from '@mui/material';

function App() {
  const { isLoading, error, isAuthenticated } = useAuth0();
  // const dispatch = useDispatch()
  // const { token } = useSelector((state) => state.token)

  GetIdToken();
  // const token = sessionStorage.getItem("id_token")
  // console.log("TOKEN: ", token)
  const token = Cookies.get('idToken');
  console.log('APP TOKEN: ', token);

  return (
    <>
      <Header />
      <div className="App App-body">
        {!isAuthenticated && <AuthenticationButton />}
        {error && <p>Authentication Error</p>}
        {!error && isLoading && <p>Loading...</p>}
        {!error && !isLoading && (
          <>
            <BrowserRouter>
              <Routes>
                <Route>
                  <Route path="/" element={<Dashboard />} />
                  <Route path="/students" element={<Student />} />
                  <Route path="/students/add" element={<StudentCreate />} />
                  <Route path="/students/:id" element={<StudentDetail />} />
                  <Route path="/projects" element={<Project />} />
                </Route>
              </Routes>
            </BrowserRouter>
          </>
        )}
      </div>
    </>
  );
}

export default App;

export const AUTH0_DOMAIN = process.env.REACT_APP_AUTH0_DOMAIN
export const AUTH0_CLIENT_ID = process.env.REACT_APP_AUTH0_CLIENT_ID
export const AUTH0_AUDIENCE = process.env.REACT_APP_AUTH0_AUDIENCE
export const AUTH0_SCOPE = process.env.REACT_APP_AUTH0_SCOPE
export const API_BASE = process.env.REACT_APP_API_BASE

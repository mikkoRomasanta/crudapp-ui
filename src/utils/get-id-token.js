import { useAuth0 } from "@auth0/auth0-react"
// import { useEffect, useState } from "react"
// import { useDispatch, useSelector } from "react-redux"
import Cookies from "js-cookie"

function GetIdToken() {
    const { getIdTokenClaims } = useAuth0()
    // const [idToken, setIdToken] = useState()
    const idToken = Cookies.get("idToken")

    if (idToken === null || idToken === undefined)
        getIdTokenClaims()
            .then((claims) => {
                // setIdToken(claims.__raw)
                // sessionStorage.setItem("id_token", claims.__raw)
                Cookies.set("idToken", claims.__raw, { expires: 1 })
                // console.log("CLAIMS: ", idToken)
            })
            .catch((err) => {
                console.debug("Something went wrong. ", err)
            })
}

export default GetIdToken

import axios from "axios"
import { API_BASE } from "./constants"
import Cookies from "js-cookie"

const onError = (error) => {
    switch (error && error.response && error.response.status) {
        case 400:
            console.error(error.message, error.response)
            break
        case 401:
        case 403:
            // sessionStorage.clear()
            window.location.href = "/"
            break
        case 500:
            console.error(
                "We've encountered a problem. Please try again later."
            )
            break
        default:
            break
    }

    return Promise.reject(error)
}

const apiRequest = async (options) => {
    // const token = sessionStorage.getItem("id_token")
    const token = Cookies.get("idToken")

    const axiosInstance = axios.create({
        baseURL: API_BASE,
        headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
        },
    })

    return axiosInstance(options)
        .then((response) => response.data)
        .catch(onError)
}

export default apiRequest

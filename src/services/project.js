import api from "../utils/api"

export const getProjectList = () =>
    api({
        method: "get",
        url: "/project/",
    })

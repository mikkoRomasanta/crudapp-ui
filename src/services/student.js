import api from '../utils/api';

export const getStudentList = () =>
  api({
    method: 'get',
    url: '/student/',
  });

export const getDetail = (id) =>
  api({
    method: 'get',
    url: '/student/' + id,
  });

export const postStudent = (data) =>
  api({
    method: 'post',
    url: '/student/',
    data: data,
  });

export const deleteStudent = (id) =>
  api({
    method: 'delete',
    url: '/student/' + id,
  });

export const patchStudent = (id, data) =>
  api({
    method: 'patch',
    url: '/student/' + id + '/',
    data: data,
  });

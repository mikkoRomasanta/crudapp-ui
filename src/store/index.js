import { configureStore } from '@reduxjs/toolkit';
// import tokenReducer from "../utils/token"
import data from '../reducers';

const store = configureStore({
  reducer: {
    data,
  },
});

export default store;

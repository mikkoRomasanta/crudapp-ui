import { React, useEffect, useState, useMemo } from 'react';
import Table from '../../components/Table';
// import SimpleTable2 from '../../components/MatTable';
import AddIcon from '@mui/icons-material/Add';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getStudents } from '../../actions/student';

export const Student = () => {
  const [isLoading, setLoading] = useState(true),
    dispatch = useDispatch(),
    students = useSelector((state) => state?.data.students);

  const columns = useMemo(
    () => [
      {
        Header: 'ID',
        accessor: 'id',
        Cell: (props) => <a href={`/students/${props.value}`}>{props.value}</a>,
      },
      {
        Header: 'UUID',
        accessor: 'uuid',
      },
      {
        Header: 'Full Name',
        accessor: 'full_name',
      },
    ],
    []
  );

  // used for material react table. currently unused
  // const columns = useMemo(
  //   () => [
  //     {
  //       header: 'ID',
  //       accessorKey: 'id',
  //       Cell: (props) => <a href={`/students/${props.value}`}>{props.value}</a>,
  //     },
  //     {
  //       header: 'UUID',
  //       accessorKey: 'uuid',
  //     },
  //     {
  //       header: 'Full Name',
  //       accessorKey: 'full_name',
  //     },
  //   ],
  //   []
  // );
  useEffect(() => {
    dispatch(getStudents()).then(() => {
      setLoading(false);
    });
  }, [dispatch]);

  if (isLoading) {
    return <div>Loading...</div>;
  }

  return (
    <div className="StudentTable">
      <h1>Student Table</h1>
      <Table data={students} columns={columns} />
      {/* <SimpleTable2 data={students} columns={columns} /> */}
      <Link to="/students/add">
        <AddIcon />
      </Link>
    </div>
  );
};

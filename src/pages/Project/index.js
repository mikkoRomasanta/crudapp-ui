import React from "react"
import { useEffect, useState, useMemo } from "react"
import Table from "../../components/Table"
import { useDispatch, useSelector } from "react-redux"
import { getProjects } from "../../actions/projects"

export const Project = () => {
    const [isLoading, setLoading] = useState(true),
        dispatch = useDispatch(),
        projects = useSelector((state) => state?.data.projects)

    const columns = useMemo(() => [
        {
            Header: "ID",
            accessor: "id",
        },
        {
            Header: "Project Name",
            accessor: "project_name",
        },
    ])

    console.log("Projects: ", projects)

    useEffect(() => {
        dispatch(getProjects()).then(() => {
            setLoading(false)
        })
    }, [dispatch])

    if (isLoading) {
        return <div>Loading...</div>
    }

    return (
        <div className="ProjectTable">
            <h1>Project Table</h1>
            <Table data={projects} columns={columns} />
        </div>
    )
}

import { React, useEffect, useState } from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import './styles.scss';
import { useDispatch } from 'react-redux';
import { postStudent } from '../../actions/student';

export const StudentCreate = () => {
  const dispatch = useDispatch(),
    [firstName, setFirstName] = useState(),
    [lastName, setlastName] = useState(),
    [enrollmentDate, setEnrollmentDate] = useState(null);

  const handleSubmit = (event) => {
    event.preventDefault();
    const body = {
      first_name: firstName,
      last_name: lastName,
      enrollment_date: enrollmentDate,
    };

    dispatch(postStudent(body))
      .then(() => {
        setFirstName(null);
        setlastName(null);
        setEnrollmentDate(null);
        alert('Student Created');
      })
      .catch((error) => console.log(error));
  };

  const checkTabPress = (key) => {
    console.log('Tab', key);
  };

  // const { dirtyFields } = formState;
  // console.log(dirtyFields);

  return (
    <>
      <form onSubmit={handleSubmit}>
        <Tabs className="model-tabs" onSelect={checkTabPress}>
          <TabList>
            <Tab>Name</Tab>
            <Tab>Enrollment Date</Tab>
            <Tab>Complete</Tab>
          </TabList>
          <TabPanel>
            <label htmlFor="first_name">
              First Name:
              <input
                type="text"
                name="first_name"
                placeholder="First Name"
                onChange={(e) => setFirstName(e.target.value)}
              />
            </label>
            <br />
            <label htmlFor="last_name">
              Last Name:
              <input
                type="text"
                placeholder="Last Name"
                onChange={(e) => setlastName(e.target.value)}
              />
            </label>
          </TabPanel>
          <TabPanel>
            <label htmlFor="enrollment_date">
              Enrollment Date:
              <input
                type="date"
                onChange={(e) => setEnrollmentDate(e.target.value)}
              />
            </label>
          </TabPanel>
          <TabPanel>
            First Name: {firstName}
            <br />
            Last Name: {lastName}
            <br />
            Enrollment Date: {enrollmentDate}
            <br />
            <input type="submit" />
          </TabPanel>
        </Tabs>
      </form>
    </>
  );
};

import { Grid } from "@mui/material"
import { Link } from "react-router-dom"
import { useAuth0 } from "@auth0/auth0-react"

export const Dashboard = () => {
    const { isAuthenticated } = useAuth0()

    return (
        isAuthenticated && (
            <Grid container spacing={1}>
                <Grid item xs={12} md={6}>
                    <Link to="/students">Students</Link>
                </Grid>
                <Grid item xs={12} md={6}>
                    <Link to="/projects">Projects</Link>
                </Grid>
            </Grid>
        )
    )
}

import { React, useEffect, useState } from 'react';
import { Delete } from '@mui/icons-material';
import { useDispatch, useSelector } from 'react-redux';
import { useParams, useNavigate } from 'react-router-dom';
import {
  getStudentDetail,
  deleteStudents,
  patchStudents,
} from '../../actions/student';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { IconButton } from '@mui/material';

export const StudentDetail = () => {
  const navigate = useNavigate(),
    [isLoading, setLoading] = useState(true),
    { id } = useParams(),
    dispatch = useDispatch(),
    student = useSelector((state) => state?.data.studentDetail),
    [firstName, setFirstName] = useState(),
    [lastName, setlastName] = useState();

  useEffect(() => {
    dispatch(getStudentDetail(id)).then(() => {
      setLoading(false);
      setFirstName(student?.first_name);
      setlastName(student?.last_name);
    });
  }, [dispatch]);

  if (isLoading) {
    return <div>Loading...</div>;
  }

  const handleDeleteStudent = (event) => {
    event.preventDefault();
    dispatch(deleteStudents(id))
      .then(() => {
        navigate('/students');
      })
      .catch((e) => {
        throw e;
      });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    let body = {};

    if (firstName) {
      body = { ...body, first_name: firstName };
    }
    if (lastName) {
      body = { ...body, last_name: lastName };
    }

    dispatch(patchStudents(id, body))
      .then((res) => {
        window.location.reload();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const checkTabPress = (key) => {
    console.log('Tab', key);
  };

  return (
    <>
      <form onSubmit={handleSubmit}>
        <Tabs className="model-tabs" onSelect={checkTabPress}>
          <TabList>
            <Tab>ID</Tab>
            <Tab>First Name</Tab>
            <Tab>Last Name</Tab>
            <Tab>Status</Tab>
            <Tab>Department Name</Tab>
            <IconButton onClick={() => handleDeleteStudent()}>
              <Delete />
            </IconButton>
          </TabList>
          <TabPanel>
            <div>
              <h1>ID: {student.id}</h1>
              <h1>UUID: {student.uuid}</h1>
            </div>
          </TabPanel>
          <TabPanel>
            <label htmlFor="first_name">
              First Name:
              <input
                type="text"
                name="first_name"
                placeholder={student.first_name}
                onChange={(e) => setFirstName(e.target.value)}
              />
            </label>
          </TabPanel>
          <TabPanel>
            <label htmlFor="last_name">
              Last Name:
              <input
                type="text"
                placeholder={student.last_name}
                onChange={(e) => setlastName(e.target.value)}
              />
            </label>
          </TabPanel>
          <TabPanel>
            <h1>{student.is_active ? 'Active' : 'Inactive'}</h1>
          </TabPanel>
          <TabPanel>
            <h1>{student.department_name}</h1>
          </TabPanel>
        </Tabs>
        <input type="submit" />
      </form>
    </>
  );
};
